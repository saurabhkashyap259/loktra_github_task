package com.mate.githubtask.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mate.githubtask.R;
import com.mate.githubtask.models.Item;
import com.mate.githubtask.viewHolders.ItemViewHolder;

import java.util.List;

/**
 * Created by sasuke on 10/6/16.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<ItemViewHolder> {

    Activity activity;
    List<Item> itemList;
    LayoutInflater layoutInflater;

    public RecyclerViewAdapter(Activity activity, List<Item> itemList) {

        this.activity = activity;
        this.itemList = itemList;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View row;

        if(layoutInflater == null){

            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        row = layoutInflater.inflate(R.layout.list_view_items, parent, false);

        ItemViewHolder viewHolder = new ItemViewHolder(row);

        row.setTag(viewHolder);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder viewHolder, int position) {

        final Item i = itemList.get(viewHolder.getAdapterPosition());

        if(i != null){

            viewHolder.authorName.setText(i.getAuthor());
            viewHolder.commitDate.setText(i.getCommit());
            viewHolder.message.setText(i.getMessage());

        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}
