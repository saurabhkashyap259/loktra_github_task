package com.mate.githubtask.utils;

/**
 * Created by sasuke on 10/6/16.
 */
public class Constants {

    public static final int HomeFragmentCount = 500;

    public static final String GITHUB_COMMIT_URL = "https://api.github.com/repos/rails/rails/commits";
}
