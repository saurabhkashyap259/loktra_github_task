package com.mate.githubtask.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mate.githubtask.R;

/**
 * Created by sasuke on 10/6/16.
 */
public class ItemViewHolder extends RecyclerView.ViewHolder {

    public TextView authorName;
    public TextView commitDate;
    public TextView message;

    public ItemViewHolder(View itemView) {
        super(itemView);

        authorName = (TextView) itemView.findViewById(R.id.author_name);
        commitDate = (TextView) itemView.findViewById(R.id.commit_date);
        message = (TextView) itemView.findViewById(R.id.message);
    }
}
