package com.mate.githubtask.models;

/**
 * Created by sasuke on 10/6/16.
 */
public class Item {

    String author;
    String commit;
    String message;

    public Item() {
    }

    public Item(String author, String commit, String message) {
        this.author = author;
        this.commit = commit;
        this.message = message;
    }

    public String getAuthor() {
        return author;
    }

    public String getCommit() {
        return commit;
    }

    public String getMessage() {
        return message;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setCommit(String commit) {
        this.commit = commit;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
