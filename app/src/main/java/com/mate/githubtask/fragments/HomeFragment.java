package com.mate.githubtask.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.mate.githubtask.R;
import com.mate.githubtask.adapters.RecyclerViewAdapter;
import com.mate.githubtask.appController.AppController;
import com.mate.githubtask.models.Item;
import com.mate.githubtask.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sasuke on 8/6/16.
 */
public class HomeFragment extends Fragment {

    private static final String TAG = HomeFragment.class.getSimpleName();

    private static RecyclerView recyclerView;
    private static List<Item> itemList = new ArrayList<>();
    private static RecyclerViewAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        return rootView;
    }

    @Override
    public void onViewCreated(View rootView, Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);

        //Local variables
        RecyclerView.LayoutManager layoutManager;

        //Layout manager
        layoutManager = new LinearLayoutManager(getActivity());

        //set layout manager
        recyclerView.setLayoutManager(layoutManager);

        //adapter
        adapter = new RecyclerViewAdapter(getActivity(), itemList);

        //set adapter
        recyclerView.setAdapter(adapter);

        class SendJsonDataRequest extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... params) {

                JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Constants.GITHUB_COMMIT_URL, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for(int i=0; i<response.length(); i++) {

                            try {

                                JSONObject jsonObject = response.getJSONObject(i);

                                JSONObject commit = jsonObject.getJSONObject("commit");

                                JSONObject author = commit.getJSONObject("author");

                                Item item = new Item();

                                item.setAuthor(author.getString("name"));
                                item.setCommit(author.getString("date"));
                                item.setMessage(commit.getString("message"));

                                itemList.add(item);

                                adapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        VolleyLog.e(TAG, "Error: " + error.getMessage());
                        Log.e("Json Array Error","Not Working");
                    }
                });

                AppController.getInstance().addToRequestQueue(jsonArrayRequest);
                return null;
            }
        }

        new SendJsonDataRequest().execute();

    }

}
